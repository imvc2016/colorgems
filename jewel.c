/*
 * Copyright (c) 1996-1999  Silicon Graphics, Inc.  All rights reserved.
 *
 * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY
 * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT SHALL SILICON GRAPHICS BE LIABLE FOR ANY SPECIAL, INCIDENTAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND, OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER OR NOT ADVISED OF THE
 * POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF LIABILITY, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* clearcolor.c - open a window and clear the background.
 */
#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>	/* includes gl.h */
#include <math.h>
#include "texture.h"

/* Function Prototypes */
GLvoid initgfx(GLvoid);
GLvoid animate(GLvoid);
GLvoid visibility(GLint);
GLvoid keyboard(GLubyte, GLint, GLint);
extern GLvoid drawScene(GLvoid);
GLvoid reshape(GLsizei, GLsizei);
GLvoid mouse(GLint, GLint, GLint, GLint);
GLvoid motion(GLint, GLint);
void resetView(GLvoid);
GLvoid initTexture( GLbyte *, GLsizei, GLsizei);
GLvoid blendFuncCycle(GLvoid);

void checkError(char * );
void printHelp( char * );

/* Global Variables */
GLfloat angle = 0.0, changeAngle = 0.0, aaa = 0.0, lightangle = 0.0;
GLfloat move = 0.0;
GLfloat scale = 0.0;
GLboolean rotateFlag = GL_TRUE;
GLboolean sphereFlag = GL_TRUE;
GLboolean decoFlag = GL_TRUE;
GLfloat r = 0.5;
int drawFlag = 0;
GLboolean lightFlag = GL_TRUE;
#define KEY_ESC 27
#define RANDOM() ((double)rand() / RAND_MAX)
enum actions { MOVE_EYE, TWIST_EYE, ZOOM, MOVE_NONE };
GLint action;
GLdouble xstart = 0.0, ystart = 0.0;
GLfloat distance = 9.5, twistAngle = 0.0, incAngle = 0.0, azimAngle = 0.0;
GLuint texnames[7];
GLUquadricObj *quadObj;
double t = 0.0;  //時刻
double dt = 0.01; //時間刻み
double tn = 0;      //ステップ数
double g = 9.80665; //重力加速度 g
double x = 0.0, y = 0.0, z = 0.0;
double vx = 0.0, vy = 0.0, vz = 0.0;
double ax = 0.0, ay = 0.0, az = 0.0;

static char *progname; 

void
main( int argc, char *argv[] )
{
	char        *imageFileName = "jgreen.rgb";
	char		*imageFileName2 ="jblue.rgb";
	char		*imageFileName3 = "jwhite.rgb";
	char		*imageFileName4 = "jyellow.rgb";
	char		*imageFileName5 = "jred.rgb";
	char		*imageFileName6 = "jcyan.rgb";
	char		*imageFileName7 = "jmazenta.rgb";
    GLubyte     *image, *image2, *image3, *image4, *image5, *image6, *image7;
    GLsizei     width, height;
    GLsizei     imageWidth, imageHeight, components, 
				imageWidth2, imageHeight2, components2,
				imageWidth3, imageHeight3, components3,
				imageWidth4, imageHeight4, components4,
				imageWidth5, imageHeight5, components5,
				imageWidth6, imageHeight6, components6,
				imageWidth7, imageHeight7, components7;


	glutInit( &argc, argv );

	image = (GLubyte * ) read_texture(imageFileName, &imageWidth,
                                     &imageHeight, &components);

	image2 = (GLubyte * ) read_texture(imageFileName2, &imageWidth2, &imageHeight2, &components2);

	image3 = (GLubyte * ) read_texture(imageFileName3, &imageWidth3, &imageHeight3, &components3);

	image4 = (GLubyte * ) read_texture(imageFileName4, &imageWidth4, &imageHeight4, &components4);

	image5 = (GLubyte * ) read_texture(imageFileName5, &imageWidth5, &imageHeight5, &components5);

	image6 = (GLubyte * ) read_texture(imageFileName6, &imageWidth6, &imageHeight6, &components6);

	image7 = (GLubyte * ) read_texture(imageFileName7, &imageWidth7, &imageHeight7, &components7);

/* create a window that is 1/4 the size of the screen,
	 * and position it in the middle of the screen.
	 */
	width = glutGet(GLUT_SCREEN_WIDTH); 
	height = glutGet( GLUT_SCREEN_HEIGHT );
	glutInitWindowPosition(width / 4, height / 4);
	glutInitWindowSize(width / 2, height / 2);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutCreateWindow(argv[0]);

	glGenTextures(7, texnames);
	initTexture(texnames[0], image, imageWidth, imageHeight);
	initTexture(texnames[1], image2, imageWidth2, imageHeight2);
	initTexture(texnames[2], image3, imageWidth3, imageHeight3);
	initTexture(texnames[3], image4, imageWidth4, imageHeight4);
	initTexture(texnames[4], image5, imageWidth5, imageHeight5);
	initTexture(texnames[5], image6, imageWidth6, imageHeight6);
	initTexture(texnames[6], image7, imageWidth7, imageHeight7);
	initgfx();

	glutDisplayFunc(drawScene); 
	glutKeyboardFunc(keyboard);
	glutIdleFunc(animate);
	glutVisibilityFunc(visibility);
	printHelp(argv[0]);
	glutReshapeFunc(reshape);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);

	progname = argv[0];

	//printHelp(progname);

	glutMainLoop();
}

GLvoid initTexture(GLuint texname, GLubyte *image, 
    GLsizei imageWidth, GLsizei imageHeight)
{
	glBindTexture(GL_TEXTURE_2D, texname);
    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, imageWidth, imageHeight,
                     GL_RGBA, GL_UNSIGNED_BYTE, image);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, 
					GL_LINEAR_MIPMAP_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}

GLvoid  
blendFuncCycle(GLvoid)
{
    static int whichBlendFunc = 0;
    
    whichBlendFunc = (whichBlendFunc + 1) % 3;
    
    switch (whichBlendFunc)
    {
    case 0:
         glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
         fprintf(stdout, 
             "glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA )\n");
         break;
    case 1:
         glBlendFunc(GL_SRC_ALPHA, GL_ONE);
         fprintf(stdout, 
             "glBlendFunc(GL_SRC_ALPHA, GL_ONE)\n");
         break;
    case 2:
         glBlendFunc(GL_DST_ALPHA, GL_DST_ALPHA);
         fprintf(stdout, 
             "glBlendFunc( GL_DST_ALPHA, GL_DST_ALPHA )\n");
         break;
    default:
         break;
    }
}

void
printHelp(char *progname)
{
	fprintf(stdout, 
		"\n%s - open a window and clear the background\n\n"
		"demonstrates how to handle keyboard input\n"
		"SPACE Key - generates a random background color\n"
		"Escape Key - プログラムを終了\n"
		"[a] - 形状1（ブリリアント）\n"
		"[b] - 形状2（スクエア）\n"
		"[c] - 形状3（ハート）\n"
		"[d] - 形状4（マーキス）\n"
		"[e] - 形状5（トリリアント）\n"
		"[k] - 背景ON OFF\n"
		"[l] - ライト切り替え\n"
		"[r] - 一時停止\n"
		"[s] - 視点の初期化\n"
		"[x] - 混合係数切り替え\n"
		"[+] - 回転速度上昇\n"
		"[-] - 回転速度減少\n\n",
		progname);
}

GLvoid
initgfx( GLvoid )
{
	/* set clear color to magenta */
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glEnable(GL_DEPTH_TEST);
	glPolygonOffset(1.0, 1.0);
	glEnable(GL_LIGHT0);
	resetView();

	quadObj = gluNewQuadric();
	gluQuadricDrawStyle(quadObj, GLU_FILL);
	gluQuadricTexture(quadObj, GL_TRUE);
	glShadeModel(GL_SMOOTH);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

GLvoid
reshape(GLsizei width, GLsizei height)
{
	GLdouble aspect, left, right, bottom, top;

	glViewport(0, 0, width, height);

	aspect = (GLdouble) width / (GLdouble) height;

	if(aspect < 1.0){
		left = -3.0;
		right = 3.0;
		bottom = -3.0 * (1.0 / aspect);
		top = 3.0 * (1.0 / aspect);
	}else{
		bottom = -3.0;
		top = 3.0;
		left = -3.0 * aspect;
		right = 3.0 * aspect;
	}
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(left, right, bottom, top, 20.0, 1000.0);
	glMatrixMode(GL_MODELVIEW);
}

void 
checkError(char * label)
{
	GLenum error;
	while ((error = glGetError()) != GL_NO_ERROR)
		printf("%s: %s\n", label, gluErrorString(error));
}

GLvoid
keyboard(GLubyte key, GLint x, GLint y)
{
	switch (key) {
	case KEY_ESC:
		exit(0);
		break;
	case' ':
		glClearColor(RANDOM(), RANDOM(), RANDOM(), 1.0);
		glutPostRedisplay();
		break;
	case'r':
		rotateFlag = !rotateFlag;
		if(rotateFlag){
			glutIdleFunc(animate);
		}else{
			glutIdleFunc(NULL);
		}
		break;
	case's':
		resetView();
		glutPostRedisplay();
	break;
	case'a':
		drawFlag = 0;
		break;
	case'b':
		drawFlag = 1;
		break;
	case'c':
		drawFlag = 2;
		break;
	case'd':
		drawFlag = 3;
		break;
	case'e':
		drawFlag = 4;
		break;
	case'k':
		decoFlag = !decoFlag;
		break;
	case'+':
		r += 0.5;
		if (r > 20.0) {
			r = 20.0;
		}
		fprintf(stdout, "回転速度＝%f\n", r);
		break;
	case'-':
		r -= 0.5;
		if (r < 0.0) {
			r = 0.0;
		}
		fprintf(stdout, "回転速度＝%f\n", r);
		break;
	case'l':
		lightFlag = !lightFlag;
		if (lightFlag){
			glEnable(GL_LIGHT1);
			glEnable(GL_LIGHT2);
			glEnable(GL_LIGHT3);
			glDisable(GL_LIGHT0);
		} else {
			glEnable(GL_LIGHT0);
			glDisable(GL_LIGHT1);
			glDisable(GL_LIGHT2);
			glDisable(GL_LIGHT3);
		}
		break;
	case'x':
		blendFuncCycle();
        glutPostRedisplay();
		break;
	}
}

GLvoid
mouse(GLint button, GLint state, GLint x, GLint y)
{
	static GLint buttons_down = 0;

	if (state == GLUT_DOWN) {
		buttons_down++;
		switch (button) {
		case GLUT_LEFT_BUTTON:
			if (glutGetModifiers() == GLUT_ACTIVE_SHIFT) {
				action = TWIST_EYE;
			} else {
				action = MOVE_EYE;
			}
			break;
		case GLUT_MIDDLE_BUTTON:
			action = TWIST_EYE;
			break;
		case GLUT_RIGHT_BUTTON:
			action = ZOOM;
			break;
		}

		xstart = x;
		ystart = y;
	} else {
		if (--buttons_down == 0) {
			action = MOVE_NONE;
		}
	}
}

GLvoid
motion(GLint x, GLint y)
{
	switch (action){
	case MOVE_EYE:
		azimAngle += (GLdouble) (x - xstart);
		incAngle -= (GLdouble) (y - ystart);
		break;
	case TWIST_EYE:
		twistAngle = fmod(twistAngle + (x - xstart), 360.0);
		break;
	case ZOOM:
		distance -= (GLdouble) (y - ystart) / 10.0;
		break;
	default:
		printf("unknown action %d/n", action);
	}

	xstart = x;
	ystart = y;

	glutPostRedisplay();
}

GLvoid
animate(GLvoid)
{
	angle = fmod((angle + 0.0 + r), 360.0);
	aaa = fmod((aaa + 0.1), 360.0);
	changeAngle = fmod((changeAngle + cos(aaa)*cos(aaa) + 0.5), 360.0);
	lightangle = fmod((lightangle + 10.0), 360.0);

	/*if (move > 2.0 || move < -2.0) {
		move = -move;
	}
	move = move + (-0.1);

	if (scale > 0.9 || scale < -0.9) {
		scale = -scale;
	}
	scale = scale + 0.1;*/

	ay = -g;
	t = dt * tn;
	vx = vx + ax * dt;
	vy = vy + ay * dt;
	vz = vz + az * dt;
	x = x + vx * dt;
	y = y + vy * dt;
	z = z + vz * dt;
	tn++;

	glutPostRedisplay();
}

GLvoid
visibility(int state)
{
	if (state == GLUT_VISIBLE && rotateFlag) {
		glutIdleFunc(animate);
	} else {
		glutIdleFunc(NULL);
	}
}



void 
resetView(GLvoid)
{
	distance = 50.0;
	twistAngle = 0.0;
	incAngle = 0.0;
	azimAngle = 0.0;
}

void polarview(GLfloat distance, GLfloat azimuth, GLfloat incidence,
	GLfloat twist)
{
	glTranslatef(0.0, 0.0, -distance);
	glRotatef(-twist, 0.0, 0.0, 1.0);
	glRotatef(-incidence, 1.0, 0.0, 0.0);
	glRotatef(-azimuth, 0.0, 0.0, 1.0);
}