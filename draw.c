#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>	/* includes gl.h */
#include <math.h>
#include "texture.h"

extern GLvoid drawScene(GLvoid);
extern void polarview(GLfloat distance, GLfloat azimuth, GLfloat incidence, GLfloat twist);
extern void checkError(char *);

extern GLfloat angle, changeAngle, aaa, lightangle;
extern GLfloat move;
extern GLfloat scale;
extern GLboolean rotateFlag;
extern GLboolean sphereFlag;
extern GLboolean decoFlag;
extern GLfloat r;
extern int drawFlag;
extern GLboolean lightFlag;
extern enum actions { MOVE_EYE, TWIST_EYE, ZOOM, MOVE_NONE };
extern GLint action;
extern GLdouble xstart, ystart;
extern GLfloat distance, twistAngle, incAngle, azimAngle;
extern GLuint texnames[7];
extern GLUquadricObj *quadObj;
extern double t;  //時刻
extern double dt; //時間刻み
extern double tn;      //ステップ数
extern double g; //重力加速度 g
extern double x, y, z;
extern double vx, vy, vz;
extern double ax, ay, az;

GLvoid
drawBrilliant(GLvoid)
{
	static GLfloat  brilliantVertex1[] = {
		-0.3, 0.0, 0.6,
		-0.6, 0.0, 0.0,
		-0.3, 0.0, -0.6,
		0.3, 0.0, -0.6,
		0.6, 0.0, 0.0,
		0.3, 0.0, 0.6
	};
	static GLfloat  brilliantTexCoord1[] = {
		0.3, 0.0,
		0.7, 0.0,
		1.0, 0.5,
		0.7, 1.0,
		0.3, 1.0,
		0.0, 0.5
	};
	glNormal3f(0.0, 1.0, 0.0);
	glVertexPointer(3, GL_FLOAT, 3 * sizeof(GLfloat), brilliantVertex1);
	glEnableClientState(GL_VERTEX_ARRAY);
	glTexCoordPointer(2, GL_FLOAT, 2 * sizeof(GLfloat), brilliantTexCoord1);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glDrawArrays(GL_POLYGON, 0, 6);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	static GLfloat  brilliantVertex2[] = {
		-0.3, 0.0, 0.6,
		-0.5, -0.3, 0.8,
		0.3, 0.0, 0.6,
		0.5, -0.3, 0.8,
		0.6, 0.0, 0.0,
		0.8, -0.3, 0.0,
		0.3, 0.0, -0.6,
		0.5, -0.3, -0.8,
		-0.3, 0.0, -0.6,
		-0.5, -0.3, -0.8,
		-0.6, 0.0, 0.0,
		-0.8, -0.3, 0.0,
		-0.3, 0.0, 0.6,
		//-0.5, -0.3, 0.8
	};
	static GLuint   brilliantindices2[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 1};
	static GLfloat  brilliantTexCoord2[] = {
		0.0, 1.0,
		0.0, 0.9,
		0.1, 1.0,
		0.1, 0.9,
		0.2, 1.0,
		0.2, 0.9,
		0.3, 1.0,
		0.3, 0.9,
		0.4, 1.0,
		0.4, 0.9,
		0.5, 1.0,
		0.5, 0.9,
		0.6, 1.0,
		0.6, 0.9
	};
	static GLfloat  brilliantNormal2[] = {
		0.0, 0.2, 1.0,
		0.0, 0.2, 1.0,
		0.0, 0.2, 1.0,
		0.0, 0.2, 1.0,
		0.5, 0.2, 1.0,
		0.5, 0.2, 1.0,
		0.5, 0.2, -1.0,
		0.5, 0.2, -1.0,
		0.0, 0.2, -1.0,
		0.0, 0.2, -1.0,
		-0.5, 0.2, -1.0,
		-0.5, 0.2, -1.0,
		-0.5, 0.2, 1.0,
		-0.5, 0.2, 1.0
	};

	glVertexPointer(3, GL_FLOAT, 3 * sizeof(GLfloat), brilliantVertex2);
	glEnableClientState(GL_VERTEX_ARRAY);
	glTexCoordPointer(2, GL_FLOAT, 2 * sizeof(GLfloat), brilliantTexCoord2);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glNormalPointer(GL_FLOAT, 3 * sizeof(GLfloat), brilliantNormal2);
	glEnableClientState(GL_NORMAL_ARRAY);
	glDrawElements(GL_TRIANGLE_STRIP, 14, GL_UNSIGNED_INT, brilliantindices2);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);

	static GLfloat  brilliantVertex3[] = {
		0.0, -0.9, 0.0,
		-0.5, -0.3, 0.8,
		0.5, -0.3, 0.8,
		0.8, -0.3, 0.0,
		0.5, -0.3, -0.8,
		-0.5, -0.3, -0.8,
		-0.8, -0.3, 0.0,
		-0.5, -0.3, 0.8
	};
	static GLuint   brilliantindices3[] = {0, 1, 2, 3, 4, 5, 6, 7};
	static GLfloat  brilliantTexCoord3[] = {
		
		0.5, 0.5,
		0.4, 0.4,
		0.6, 0.4,
		0.7, 0.5,
		0.6, 0.6,
		0.4, 0.6,
		0.3, 0.5,
		0.4, 0.4
	};
	static GLfloat  brilliantNormal3[] = {
		0.0, -0.2, 1.0,
		0.0, -0.2, 1.0,
		0.0, -0.2, 1.0,
		0.5, -0.2, 1.0,
		0.5, -0.2, -1.0,
		0.0, -0.2, -1.0,
		-0.5, -0.2, -1.0,
		-0.5, -0.2, 1.0
	};
	glVertexPointer(3, GL_FLOAT, 3 * sizeof(GLfloat), brilliantVertex3);
	glEnableClientState(GL_VERTEX_ARRAY);
	glTexCoordPointer(2, GL_FLOAT, 2 * sizeof(GLfloat), brilliantTexCoord3);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glNormalPointer(GL_FLOAT, 3 * sizeof(GLfloat), brilliantNormal3);
	glEnableClientState(GL_NORMAL_ARRAY);
	glDrawElements(GL_TRIANGLE_FAN, 8, GL_UNSIGNED_INT, brilliantindices3);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);

	/*glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0, 1.0, 1.0);
	glNormal3f(0.0, -0.2, 1.0);
	glTexCoord2f(0.5, 0.5); glVertex3f(0.0, -0.9, 0.0);
	glTexCoord2f(0.4, 0.4); glVertex3f(-0.5, -0.3, 0.8);
	glTexCoord2f(0.6, 0.4); glVertex3f(0.5, -0.3, 0.8);
	glNormal3f(0.5, -0.2, 1.0);
	glTexCoord2f(0.7, 0.5); glVertex3f(0.8, -0.3, 0.0);
	glNormal3f(0.5, -0.2, -1.0);
	glTexCoord2f(0.6, 0.6); glVertex3f(0.5, -0.3, -0.8);
	glNormal3f(0.0, -0.2, -1.0);
	glTexCoord2f(0.4, 0.6); glVertex3f(-0.5, -0.3, -0.8);
	glNormal3f(-0.5, -0.2, -1.0);
	glTexCoord2f(0.3, 0.5); glVertex3f(-0.8, -0.3, 0.0);
	glNormal3f(-0.5, -0.2, 1.0);
	glTexCoord2f(0.4, 0.4); glVertex3f(-0.5, -0.3, 0.8);
	glEnd();*/
}

GLvoid
drawSquare(GLvoid)
{
	glBegin(GL_POLYGON);
	glNormal3f(0.0, 0.0, 1.0);
	glTexCoord2f(0.2, 1.0); glVertex3f(-0.3, 0.6, 0.0);
	glTexCoord2f(0.0, 0.8); glVertex3f(-0.5, 0.4, 0.0);
	glTexCoord2f(0.0, 0.2); glVertex3f(-0.5, -0.4, 0.0);
	glTexCoord2f(0.2, 0.0); glVertex3f(-0.3, -0.6, 0.0);
	glTexCoord2f(0.8, 0.0); glVertex3f(0.3, -0.6, 0.0);
	glTexCoord2f(1.0, 0.2); glVertex3f(0.5, -0.4, 0.0);
	glTexCoord2f(1.0, 0.8); glVertex3f(0.5, 0.4, 0.0);
	glTexCoord2f(0.8, 1.0); glVertex3f(0.3, 0.6, 0.0);
	glEnd();

	glBegin(GL_QUAD_STRIP);
	glNormal3f(0.0, 0.1, 1.0);
	glTexCoord2f(0.0, 1.0); glVertex3f(-0.3, -0.6, 0.0);
	glTexCoord2f(0.0, 0.9); glVertex3f(-0.4, -0.7, -0.1);
	glTexCoord2f(0.1, 1.0); glVertex3f(0.3, -0.6, 0.0);
	glTexCoord2f(0.1, 0.9); glVertex3f(0.4, -0.7, -0.1);
	glNormal3f(0.2, 0.1, 1.0);
	glTexCoord2f(0.2, 1.0); glVertex3f(0.5, -0.4, 0.0);
	glTexCoord2f(0.2, 0.9); glVertex3f(0.6, -0.5, -0.1);
	glNormal3f(1.0, 0.1, 0.0);
	glTexCoord2f(0.3, 1.0); glVertex3f(0.5, 0.4, 0.0);
	glTexCoord2f(0.3, 0.9); glVertex3f(0.6, 0.5, -0.1);
	glNormal3f(0.2, 0.1, -1.0);
	glTexCoord2f(0.4, 1.0); glVertex3f(0.3, 0.6, 0.0);
	glTexCoord2f(0.4, 0.9); glVertex3f(0.4, 0.7, -0.1);
	glNormal3f(0.0, 0.1, -1.0);
	glTexCoord2f(0.5, 1.0); glVertex3f(-0.3, 0.6, 0.0);
	glTexCoord2f(0.5, 0.9); glVertex3f(-0.4, 0.7, -0.1);
	glNormal3f(-0.2, 0.1, -1.0);
	glTexCoord2f(0.6, 1.0); glVertex3f(-0.5, 0.4, 0.0);
	glTexCoord2f(0.6, 0.9); glVertex3f(-0.6, 0.5, -0.1);
	glNormal3f(-1.0, 0.1, 0.0);
	glTexCoord2f(0.7, 1.0); glVertex3f(-0.5, -0.4, 0.0);
	glTexCoord2f(0.7, 0.9); glVertex3f(-0.6, -0.5, -0.1);
	glNormal3f(-0.2, 0.1, 1.0);
	glTexCoord2f(0.8, 1.0); glVertex3f(-0.3, -0.6, 0.0);
	glTexCoord2f(0.8, 0.9); glVertex3f(-0.4, -0.7, -0.1);
	glEnd();

	glBegin(GL_QUAD_STRIP);
	glNormal3f(0.0, -0.1, 1.0);
	glTexCoord2f(0.0, 1.0); glVertex3f(-0.4, -0.7, -0.1);
	glTexCoord2f(0.0, 0.9); glVertex3f(-0.3, -0.6, -0.2);
	glTexCoord2f(0.1, 1.0); glVertex3f(0.4, -0.7, -0.1);
	glTexCoord2f(0.1, 0.9); glVertex3f(0.3, -0.6, -0.2);
	glNormal3f(0.2, -0.1, 1.0);
	glTexCoord2f(0.2, 1.0); glVertex3f(0.6, -0.5, -0.1);
	glTexCoord2f(0.2, 0.9); glVertex3f(0.5, -0.4, -0.2);
	glNormal3f(1.0, -0.1, 0.0);
	glTexCoord2f(0.3, 1.0); glVertex3f(0.6, 0.5, -0.1);
	glTexCoord2f(0.3, 0.9); glVertex3f(0.5, 0.4, -0.2);
	glNormal3f(0.2, -0.1, 1.0);
	glTexCoord2f(0.4, 1.0); glVertex3f(0.4, 0.7, -0.1);
	glTexCoord2f(0.4, 0.9); glVertex3f(0.3, 0.6, -0.2);
	glNormal3f(0.0, -0.1, -1.0);
	glTexCoord2f(0.5, 1.0); glVertex3f(-0.4, 0.7, -0.1);
	glTexCoord2f(0.5, 0.9); glVertex3f(-0.3, 0.6, -0.2);
	glNormal3f(-0.2, -0.1, -1.0);
	glTexCoord2f(0.6, 1.0); glVertex3f(-0.6, 0.5, -0.1);
	glTexCoord2f(0.6, 0.9); glVertex3f(-0.5, 0.4, -0.2);
	glNormal3f(-1.0, -0.1, 0.0);
	glTexCoord2f(0.7, 1.0); glVertex3f(-0.6, -0.5, -0.1);
	glTexCoord2f(0.7, 0.9); glVertex3f(-0.5, -0.4, -0.2);
	glNormal3f(-0.2, -0.1, 1.0);
	glTexCoord2f(0.8, 1.0); glVertex3f(-0.4, -0.7, -0.1);
	glTexCoord2f(0.8, 0.9); glVertex3f(-0.3, -0.6, -0.2);
	glEnd();

	glBegin(GL_POLYGON);
	glNormal3f(0.0, 0.0, -1.0);
	glTexCoord2f(0.2, 1.0); glVertex3f(-0.3, 0.6, -0.2);
	glTexCoord2f(0.0, 0.8); glVertex3f(-0.5, 0.4, -0.2);
	glTexCoord2f(0.0, 0.2); glVertex3f(-0.5, -0.4, -0.2);
	glTexCoord2f(0.2, 0.0); glVertex3f(-0.3, -0.6, -0.2);
	glTexCoord2f(0.8, 0.0); glVertex3f(0.3, -0.6, -0.2);
	glTexCoord2f(1.0, 0.2); glVertex3f(0.5, -0.4, -0.2);
	glTexCoord2f(1.0, 0.8); glVertex3f(0.5, 0.4, -0.2);
	glTexCoord2f(0.8, 1.0); glVertex3f(0.3, 0.6, -0.2);
	glEnd();
}

GLvoid
drawHeart(GLvoid)
{
	glBegin(GL_POLYGON);
	glNormal3f(0.0, 0.0, 1.0);
	glTexCoord2f(0.5, 0.0); glVertex3f(0.0, -0.5, 0.0);
	glTexCoord2f(0.3, 0.5); glVertex3f(-0.4, 0.0, 0.0);
	glTexCoord2f(0.3, 0.7); glVertex3f(-0.4, 0.2, 0.0);
	glTexCoord2f(0.35, 0.8); glVertex3f(-0.3, 0.3, 0.0);
	glTexCoord2f(0.4, 0.9); glVertex3f(-0.2, 0.4, 0.0);
	glTexCoord2f(0.5, 0.8); glVertex3f(-0.1, 0.3, 0.0);
	glTexCoord2f(0.55, 0.7); glVertex3f(0.0, 0.2, 0.0);
	glTexCoord2f(0.6, 0.8); glVertex3f(0.1, 0.3, 0.0);
	glTexCoord2f(0.65, 0.9); glVertex3f(0.2, 0.4, 0.0);
	glTexCoord2f(0.7, 0.8); glVertex3f(0.3, 0.3, 0.0);
	glTexCoord2f(0.8, 0.7); glVertex3f(0.4, 0.2, 0.0);
	glTexCoord2f(0.8, 0.6); glVertex3f(0.4, 0.0, 0.0);
	glEnd();

	glBegin(GL_QUAD_STRIP);
	glNormal3f(-0.8, 0.2, 0.5);
	glTexCoord2f(0.0, 1.0); glVertex3f(0.0, -0.5, 0.0);
	glTexCoord2f(0.0, 0.9); glVertex3f(0.0, -0.6, -0.1);
	glTexCoord2f(0.15, 1.0); glVertex3f(-0.4, 0.0, 0.0);
	glTexCoord2f(0.15, 0.9); glVertex3f(-0.5, 0.0, -0.1);
	glNormal3f(-1.0, 0.2, 0.0);
	glTexCoord2f(0.1, 1.0); glVertex3f(-0.4, 0.2, 0.0);
	glTexCoord2f(0.1, 0.9); glVertex3f(-0.5, 0.2, -0.1);
	glNormal3f(-0.5, 0.2, -0.5);
	glTexCoord2f(0.15, 1.0); glVertex3f(-0.3, 0.3, 0.0);
	glTexCoord2f(0.15, 0.9); glVertex3f(-0.3, 0.4, -0.1);
	glNormal3f(-0.5, 0.2, -0.6);
	glTexCoord2f(0.2, 1.0); glVertex3f(-0.2, 0.4, 0.0);
	glTexCoord2f(0.2, 0.9); glVertex3f(-0.2, 0.5, -0.1);
	glNormal3f(0.5, 0.2, -0.5);
	glTexCoord2f(0.25, 1.0); glVertex3f(-0.1, 0.3, 0.0);
	glTexCoord2f(0.25, 0.9); glVertex3f(-0.1, 0.4, -0.1);
	glNormal3f(0.5, 0.2, -0.6);
	glTexCoord2f(0.3, 1.0); glVertex3f(0.0, 0.2, 0.0);
	glTexCoord2f(0.3, 0.9); glVertex3f(0.0, 0.3, -0.1);
	glNormal3f(-0.5, 0.2, -0.6);
	glTexCoord2f(0.35, 1.0); glVertex3f(0.1, 0.3, 0.0);
	glTexCoord2f(0.35, 0.9); glVertex3f(0.1, 0.4, -0.1);
	glNormal3f(-0.5, 0.2, -0.5);
	glTexCoord2f(0.4, 1.0); glVertex3f(0.2, 0.4, 0.0);
	glTexCoord2f(0.4, 0.9); glVertex3f(0.2, 0.5, -0.1);
	glNormal3f(0.5, 0.2, -0.6);
	glTexCoord2f(0.45, 1.0); glVertex3f(0.3, 0.3, 0.0);
	glTexCoord2f(0.45, 0.9); glVertex3f(0.3, 0.4, -0.1);
	glNormal3f(0.5, 0.2, -0.5);
	glTexCoord2f(0.5, 1.0); glVertex3f(0.4, 0.2, 0.0);
	glTexCoord2f(0.5, 0.9); glVertex3f(0.5, 0.2, -0.1);
	glNormal3f(1.0, 0.2, 0.0);
	glTexCoord2f(0.55, 1.0); glVertex3f(0.4, 0.0, 0.0);
	glTexCoord2f(0.55, 0.9); glVertex3f(0.5, 0.0, -0.1);
	glNormal3f(-0.8, 0.2, 0.5);
	glTexCoord2f(0.6, 1.0); glVertex3f(0.0, -0.5, 0.0);
	glTexCoord2f(0.6, 0.9); glVertex3f(0.0, -0.6, -0.1);
	glEnd();

	glBegin(GL_QUAD_STRIP);
	glNormal3f(-0.8, -0.2, 0.5);
	glTexCoord2f(0.0, 1.0); glVertex3f(0.0, -0.6, -0.1);
	glTexCoord2f(0.0, 0.9); glVertex3f(0.0, -0.5, -0.2);
	glTexCoord2f(0.05, 1.0); glVertex3f(-0.5, 0.0, -0.1);
	glTexCoord2f(0.05, 0.9); glVertex3f(-0.4, 0.0, -0.2);
	glNormal3f(-1.0, -0.2, 0.0);
	glTexCoord2f(0.1, 1.0); glVertex3f(-0.5, 0.2, -0.1);
	glTexCoord2f(0.1, 0.9); glVertex3f(-0.4, 0.2, -0.2);
	glNormal3f(-0.5, -0.2, -0.5);
	glTexCoord2f(0.15, 1.0); glVertex3f(-0.3, 0.4, -0.1);
	glTexCoord2f(0.15, 0.9); glVertex3f(-0.3, 0.3, -0.2);
	glNormal3f(-0.5, -0.2, -0.6);
	glTexCoord2f(0.2, 1.0); glVertex3f(-0.2, 0.5, -0.1);
	glTexCoord2f(0.2, 0.9); glVertex3f(-0.2, 0.4, -0.2);
	glNormal3f(0.5, -0.2, -0.5);
	glTexCoord2f(0.25, 1.0); glVertex3f(-0.1, 0.4, -0.1);
	glTexCoord2f(0.25, 0.9); glVertex3f(-0.1, 0.3, -0.2);
	glNormal3f(0.5, -0.2, -0.6);
	glTexCoord2f(0.3, 1.0); glVertex3f(0.0, 0.3, -0.1);
	glTexCoord2f(0.3, 0.9); glVertex3f(0.0, 0.2, -0.2);
	glNormal3f(-0.5, 0.2, -0.6);
	glTexCoord2f(0.35, 1.0); glVertex3f(0.1, 0.3, 0.0);
	glTexCoord2f(0.35, 0.9); glVertex3f(0.1, 0.2, -0.2);
	glNormal3f(-0.5, -0.2, -0.5);
	glTexCoord2f(0.4, 1.0); glVertex3f(0.2, 0.5, -0.1);
	glTexCoord2f(0.4, 0.9); glVertex3f(0.2, 0.4, -0.2);
	glNormal3f(0.5, -0.2, -0.6);
	glTexCoord2f(0.45, 1.0); glVertex3f(0.3, 0.4, -0.1);
	glTexCoord2f(0.45, 0.9); glVertex3f(0.3, 0.3, -0.2);
	glNormal3f(0.5, -0.2, -0.5);
	glTexCoord2f(0.5, 1.0); glVertex3f(0.5, 0.2, -0.1);
	glTexCoord2f(0.5, 0.9); glVertex3f(0.4, 0.2, -0.2);
	glNormal3f(1.0, -0.2, 0.0);
	glTexCoord2f(0.55, 1.0); glVertex3f(0.5, 0.0, -0.1);
	glTexCoord2f(0.55, 0.9); glVertex3f(0.4, 0.0, -0.2);
	glNormal3f(-0.8, -0.2, 0.5);
	glTexCoord2f(0.6, 1.0); glVertex3f(0.0, -0.6, -0.1);
	glTexCoord2f(0.6, 0.9); glVertex3f(0.0, -0.5, -0.2);
	glEnd();

	glBegin(GL_POLYGON);
	glNormal3f(0.0, 0.0, -1.0);
	glTexCoord2f(0.5, 0.0); glVertex3f(0.0, -0.5, -0.2);
	glTexCoord2f(0.3, 0.5); glVertex3f(-0.4, 0.0, -0.2);
	glTexCoord2f(0.3, 0.7); glVertex3f(-0.4, 0.2, -0.2);
	glTexCoord2f(0.35, 0.8); glVertex3f(-0.3, 0.3, -0.2);
	glTexCoord2f(0.4, 0.9); glVertex3f(-0.2, 0.4, -0.2);
	glTexCoord2f(0.5, 0.8); glVertex3f(-0.1, 0.3, -0.2);
	glTexCoord2f(0.55, 0.7); glVertex3f(0.0, 0.2, -0.2);
	glTexCoord2f(0.6, 0.8); glVertex3f(0.1, 0.3, -0.2);
	glTexCoord2f(0.65, 0.9); glVertex3f(0.2, 0.4, -0.2);
	glTexCoord2f(0.7, 0.8); glVertex3f(0.3, 0.3, -0.2);
	glTexCoord2f(0.8, 0.7); glVertex3f(0.4, 0.2, -0.2);
	glTexCoord2f(0.8, 0.6); glVertex3f(0.4, 0.0, -0.2);
	glEnd();
}

GLvoid
drawMarquis(GLvoid)
{
	glBegin(GL_POLYGON);
	glNormal3f(0.0, 0.0, 1.0);
	glTexCoord2f(0.0, 0.5); glVertex3f(-0.3, 0.0, 0.0);
	glTexCoord2f(0.3, 0.8); glVertex3f(-0.1, 0.4, 0.0);
	glTexCoord2f(0.5, 1.0); glVertex3f(0.0, 0.5, 0.0);
	glTexCoord2f(0.7, 0.8); glVertex3f(0.1, 0.4, 0.0);
	glTexCoord2f(1.0, 0.5); glVertex3f(0.3, 0.0, 0.0);
	glTexCoord2f(0.7, 0.3); glVertex3f(0.1, -0.4, 0.0);
	glTexCoord2f(0.5, 0.0); glVertex3f(0.0, -0.5, 0.0);
	glTexCoord2f(0.3, 0.3); glVertex3f(-0.1, -0.4, 0.0);
	glEnd();

	glBegin(GL_QUAD_STRIP);
	glNormal3f(-1.0, 0.1, 0.1);
	glTexCoord2f(0.0, 1.0); glVertex3f(-0.3, 0.0, 0.0);
	glTexCoord2f(0.0, 0.9); glVertex3f(-0.4, 0.0, -0.1);
	glTexCoord2f(0.1, 1.0); glVertex3f(-0.1, 0.4, 0.0);
	glTexCoord2f(0.1, 0.9); glVertex3f(-0.2, 0.5, -0.1);
	glNormal3f(-1.0, 0.2, 0.1);
	glTexCoord2f(0.2, 1.0); glVertex3f(0.0, 0.5, 0.0);
	glTexCoord2f(0.2, 0.9); glVertex3f(0.0, 0.6, -0.1);
	glNormal3f(1.0, 0.2, 0.1);
	glTexCoord2f(0.3, 1.0); glVertex3f(0.1, 0.4, 0.0);
	glTexCoord2f(0.3, 0.9); glVertex3f(0.2, 0.5, -0.1);
	glNormal3f(1.0, 0.1, 0.1);
	glTexCoord2f(0.4, 1.0); glVertex3f(0.3, 0.0, 0.0);
	glTexCoord2f(0.4, 0.9); glVertex3f(0.4, 0.0, -0.1);
	glNormal3f(1.0, -0.1, 0.1);
	glTexCoord2f(0.5, 1.0); glVertex3f(0.1, -0.4, 0.0);
	glTexCoord2f(0.5, 0.9); glVertex3f(0.2, -0.5, -0.1);
	glNormal3f(1.0, -0.2, 0.1);
	glTexCoord2f(0.6, 1.0); glVertex3f(0.0, -0.5, 0.0);
	glTexCoord2f(0.6, 0.9); glVertex3f(0.0, -0.6, -0.1);
	glNormal3f(-1.0, -0.2, 0.1);
	glTexCoord2f(0.7, 1.0); glVertex3f(-0.1, -0.4, 0.0);
	glTexCoord2f(0.7, 0.9); glVertex3f(-0.2, -0.5, -0.1);
	glNormal3f(-1.0, -0.1, 0.1);
	glTexCoord2f(0.8, 1.0); glVertex3f(-0.3, 0.0, 0.0);
	glTexCoord2f(0.8, 0.9); glVertex3f(-0.4, 0.0, -0.1);
	glEnd();

	glBegin(GL_QUAD_STRIP);
	glNormal3f(-1.0, 0.1, 0.0);
	glTexCoord2f(0.0, 1.0); glVertex3f(-0.4, 0.0, -0.1);
	glTexCoord2f(0.0, 0.9); glVertex3f(-0.4, 0.0, -0.2);
	glTexCoord2f(0.1, 1.0); glVertex3f(-0.2, 0.5, -0.1);
	glTexCoord2f(0.1, 0.9); glVertex3f(-0.2, 0.5, -0.2);
	glNormal3f(-1.0, 0.2, 0.0);
	glTexCoord2f(0.2, 1.0); glVertex3f(0.0, 0.6, -0.1);
	glTexCoord2f(0.2, 0.9); glVertex3f(0.0, 0.6, -0.2);
	glNormal3f(1.0, 0.2, 0.0);
	glTexCoord2f(0.3, 1.0); glVertex3f(0.2, 0.5, -0.1);
	glTexCoord2f(0.3, 0.9); glVertex3f(0.2, 0.5, -0.2);
	glNormal3f(1.0, 0.1, 0.0);
	glTexCoord2f(0.4, 1.0); glVertex3f(0.4, 0.0, -0.1);
	glTexCoord2f(0.4, 0.9); glVertex3f(0.4, 0.0, -0.2);
	glNormal3f(1.0, -0.1, 0.0);
	glTexCoord2f(0.5, 1.0); glVertex3f(0.2, -0.5, -0.1);
	glTexCoord2f(0.5, 0.9); glVertex3f(0.2, -0.5, -0.2);
	glNormal3f(1.0, -0.2, 0.0);
	glTexCoord2f(0.6, 1.0); glVertex3f(0.0, -0.6, -0.1);
	glTexCoord2f(0.6, 0.9); glVertex3f(0.0, -0.6, -0.2);
	glNormal3f(-1.0, -0.2, 0.0);
	glTexCoord2f(0.7, 1.0); glVertex3f(-0.2, -0.5, -0.1);
	glTexCoord2f(0.7, 0.9); glVertex3f(-0.2, -0.5, -0.2);
	glNormal3f(-1.0, -0.1, 0.0);
	glTexCoord2f(0.8, 1.0); glVertex3f(-0.4, 0.0, -0.1);
	glTexCoord2f(0.8, 0.9); glVertex3f(-0.4, 0.0, -0.2);
	glEnd();

	glBegin(GL_QUAD_STRIP);
	glNormal3f(-1.0, 0.1, -0.1);
	glTexCoord2f(0.0, 1.0); glVertex3f(-0.4, 0.0, -0.2);
	glTexCoord2f(0.0, 0.9); glVertex3f(-0.3, 0.0, -0.3);
	glTexCoord2f(0.1, 1.0); glVertex3f(-0.2, 0.5, -0.2);
	glTexCoord2f(0.1, 0.9); glVertex3f(-0.1, 0.4, -0.3);
	glNormal3f(-1.0, 0.2, -0.1);
	glTexCoord2f(0.2, 1.0); glVertex3f(0.0, 0.6, -0.2);
	glTexCoord2f(0.2, 0.9); glVertex3f(0.0, 0.5, -0.3);
	glNormal3f(1.0, 0.2, -0.1);
	glTexCoord2f(0.3, 1.0); glVertex3f(0.2, 0.5, -0.2);
	glTexCoord2f(0.3, 0.9); glVertex3f(0.1, 0.4, -0.3);
	glNormal3f(1.0, 0.1, -0.1);
	glTexCoord2f(0.4, 1.0); glVertex3f(0.4, 0.0, -0.2);
	glTexCoord2f(0.4, 0.9); glVertex3f(0.3, 0.0, -0.3);
	glNormal3f(1.0, -0.1, -0.1);
	glTexCoord2f(0.5, 1.0); glVertex3f(0.2, -0.5, -0.2);
	glTexCoord2f(0.5, 0.9); glVertex3f(0.1, -0.4, -0.3);
	glNormal3f(1.0, -0.2, -0.1);
	glTexCoord2f(0.6, 1.0); glVertex3f(0.0, -0.6, -0.2);
	glTexCoord2f(0.6, 0.9); glVertex3f(0.0, -0.5, -0.3);
	glNormal3f(-1.0, -0.2, -0.1);
	glTexCoord2f(0.7, 1.0); glVertex3f(-0.2, -0.5, -0.2);
	glTexCoord2f(0.7, 0.9); glVertex3f(-0.1, -0.4, -0.3);
	glNormal3f(-1.0, -0.1, -0.1);
	glTexCoord2f(0.8, 1.0); glVertex3f(-0.4, 0.0, -0.2);
	glTexCoord2f(0.8, 0.9); glVertex3f(-0.3, 0.0, -0.3);
	glEnd();

	glBegin(GL_POLYGON);
	glNormal3f(0.0, 0.0, -1.0);
	glTexCoord2f(0.0, 0.5); glVertex3f(-0.3, 0.0, -0.3);
	glTexCoord2f(0.3, 0.8); glVertex3f(-0.1, 0.4, -0.3);
	glTexCoord2f(0.5, 1.0); glVertex3f(0.0, 0.5, -0.3);
	glTexCoord2f(0.7, 0.8); glVertex3f(0.1, 0.4, -0.3);
	glTexCoord2f(1.0, 0.5); glVertex3f(0.3, 0.0, -0.3);
	glTexCoord2f(0.7, 0.3); glVertex3f(0.1, -0.4, -0.3);
	glTexCoord2f(0.5, 0.0); glVertex3f(0.0, -0.5, -0.3);
	glTexCoord2f(0.3, 0.3); glVertex3f(-0.1, -0.4, -0.3);
	glEnd();
}

GLvoid
drawTrilliant(GLvoid)
{
	glBegin(GL_POLYGON);
	glNormal3f(0.0, 0.0, 1.0);
	glTexCoord2f(0.5, 1.0); glVertex3f(0.0, 0.4, 0.0);
	glTexCoord2f(0.2, 0.4); glVertex3f(-0.4, 0.0, 0.0);
	glTexCoord2f(0.0, 0.1); glVertex3f(-0.6, -0.4, 0.0);
	glTexCoord2f(0.5, 0.0); glVertex3f(0.0, -0.6, 0.0);
	glTexCoord2f(1.0, 0.1); glVertex3f(0.6, -0.4, 0.0);
	glTexCoord2f(0.8, 0.4); glVertex3f(0.4, 0.0, 0.0);
	glEnd();

	glBegin(GL_QUAD_STRIP);
	glNormal3f(-1.0, 0.2, 0.1);
	glTexCoord2f(0.0, 1.0); glVertex3f(0.0, 0.4, 0.0);
	glTexCoord2f(0.0, 0.9); glVertex3f(0.0, 0.5, -0.1);
	glTexCoord2f(0.1, 1.0); glVertex3f(-0.4, 0.0, 0.0);
	glTexCoord2f(0.1, 0.9); glVertex3f(-0.5, 0.1, -0.1);
	glNormal3f(-1.0, 0.1, 0.1);
	glTexCoord2f(0.2, 1.0); glVertex3f(-0.6, -0.4, 0.0);
	glTexCoord2f(0.2, 0.9); glVertex3f(-0.7, -0.5, -0.1);
	glNormal3f(-0.1, -1.0, 0.1);
	glTexCoord2f(0.3, 1.0); glVertex3f(0.0, -0.6, 0.0);
	glTexCoord2f(0.3, 0.9); glVertex3f(0.0, -0.7, -0.1);
	glNormal3f(0.1, -1.0, 0.1);
	glTexCoord2f(0.4, 1.0); glVertex3f(0.6, -0.4, 0.0);
	glTexCoord2f(0.4, 0.9); glVertex3f(0.7, -0.5, -0.1);
	glNormal3f(1.0, 0.1, 0.1);
	glTexCoord2f(0.5, 1.0); glVertex3f(0.4, 0.0, 0.0);
	glTexCoord2f(0.5, 0.9); glVertex3f(0.5, 0.1, -0.1);
	glNormal3f(1.0, 0.2, 0.1);
	glTexCoord2f(0.6, 1.0); glVertex3f(0.0, 0.4, 0.0);
	glTexCoord2f(0.6, 0.9); glVertex3f(0.0, 0.5, -0.1);
	glEnd();

	glBegin(GL_QUAD_STRIP);
	glNormal3f(-1.0, 0.2, -0.1);
	glTexCoord2f(0.0, 1.0); glVertex3f(0.0, 0.5, -0.1);
	glTexCoord2f(0.0, 0.9); glVertex3f(0.0, 0.4, -0.2);
	glTexCoord2f(0.1, 1.0); glVertex3f(-0.5, 0.1, -0.1);
	glTexCoord2f(0.1, 0.9); glVertex3f(-0.4, 0.0, -0.2);
	glNormal3f(-1.0, 0.1, -0.1);
	glTexCoord2f(0.2, 1.0); glVertex3f(-0.7, -0.5, -0.1);
	glTexCoord2f(0.2, 0.9); glVertex3f(-0.6, -0.4, -0.2);
	glNormal3f(-0.1, -1.0, -0.1);
	glTexCoord2f(0.3, 1.0); glVertex3f(0.0, -0.7, -0.1);
	glTexCoord2f(0.3, 0.9); glVertex3f(0.0, -0.6, -0.2);
	glNormal3f(0.1, -1.0, -0.1);
	glTexCoord2f(0.4, 1.0); glVertex3f(0.7, -0.5, -0.1);
	glTexCoord2f(0.4, 0.9); glVertex3f(0.6, -0.4, -0.2);
	glNormal3f(1.0, 0.1, -0.1);
	glTexCoord2f(0.5, 1.0); glVertex3f(0.5, 0.1, -0.1);
	glTexCoord2f(0.5, 0.9); glVertex3f(0.4, 0.0, -0.2);
	glNormal3f(1.0, 0.2, -0.1);
	glTexCoord2f(0.6, 1.0); glVertex3f(0.0, 0.5, -0.1);
	glTexCoord2f(0.6, 0.9); glVertex3f(0.0, 0.4, -0.2);
	glEnd();

	glBegin(GL_POLYGON);
	glNormal3f(0.0, 0.0, -1.0);
	glTexCoord2f(0.5, 1.0); glVertex3f(0.0, 0.4, -0.2);
	glTexCoord2f(0.2, 0.4); glVertex3f(-0.4, 0.0, -0.2);
	glTexCoord2f(0.0, 0.1); glVertex3f(-0.6, -0.4, -0.2);
	glTexCoord2f(0.5, 0.0); glVertex3f(0.0, -0.6, -0.2);
	glTexCoord2f(1.0, 0.1); glVertex3f(0.6, -0.4, -0.2);
	glTexCoord2f(0.8, 0.4); glVertex3f(0.4, 0.0, -0.2);
	glEnd();
}

GLvoid
drawScene(GLvoid)
{
	GLfloat green[] = { 0.0, 0.6, 0.0, 0.5 };
	GLfloat blue[] = { 0.0, 0.0, 0.6, 0.5 };
	GLfloat silver[] = { 0.6, 0.6, 0.6, 0.5 };
	GLfloat yellow[] = { 0.6, 0.6, 0.0, 0.5 };
	GLfloat red[] = { 0.6, 0.0, 0.0, 0.5 };
	GLfloat cyan[] = { 0.0, 0.6, 0.6, 0.5 };
	GLfloat purple[] = { 0.6, 0.0, 0.6, 0.5 };
	GLfloat black[] = { 0.0, 0.0, 0.0, 1.0 };
	GLfloat swhite[] = { 0.2, 0.2, 0.2, 1.0 };
	GLfloat white[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat lightd1[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat lightd2[] = { 0.5, 0.5, 0.8, 1.0 };
	GLfloat lightd3[] = { 1.0, 1.0, 0.5, 1.0 };
	GLfloat lightpos1[] = { -3.0, 3.0, 0.0, 0.0 };
	GLfloat lightpos2[] = { 3.0, 3.0, 0.0, 0.0 };
	GLfloat lightpos3[] = { 0.0, 0.0, -3.0, 0.0 };

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glPushMatrix();

	glEnable(GL_LIGHTING);
	//glTranslatef(0.0, 0.0, -9.5);
	polarview(distance, azimAngle, incAngle, twistAngle);

	glLightfv(GL_LIGHT1, GL_POSITION, lightpos1);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lightd1);
	glLightfv(GL_LIGHT2, GL_POSITION, lightpos2);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, lightd2);
	glLightfv(GL_LIGHT3, GL_POSITION, lightpos3);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, lightd3);
	glPushMatrix();
	if (decoFlag){
	}
	else {
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, white);
		glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, white);
		glBegin(GL_QUADS);
		glNormal3f(0.0, 0.0, 1.0);
		glVertex3f(-5.0, -5.0, -5.0);
		glVertex3f(5.0, -5.0, -5.0);
		glVertex3f(5.0, 5.0, -5.0);
		glVertex3f(-5.0, 5.0, -5.0);
		glEnd();

		glBegin(GL_QUADS);
		glVertex3f(-5.0, -5.0, -5.0);
		glVertex3f(5.0, -5.0, -5.0);
		glVertex3f(5.0, -5.0, 5.0);
		glVertex3f(-5.0, -5.0, 5.0);
		glEnd();
	}
	glPopMatrix();
	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, green);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, white);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 128.0);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, black);
	//glScalef(2.0, 2.0, 2.0);
	//glRotatef(angle, 0.0, 1.0, 0.0);
	glTranslatef(0.0, 0.0, 0.0);
	//glRotatef(angle, 1.0, 0.0, 0.0);
	glEnable(GL_BLEND);
	glBindTexture(GL_TEXTURE_2D, texnames[0]);
	drawBrilliant();
	glPopMatrix();

	glPushMatrix();
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, blue);
	//glRotatef(angle, 0.0, 1.0, 0.0);
	glTranslatef(0.0, 0.0, 3.0);
	glBindTexture(GL_TEXTURE_2D, texnames[1]);
	drawBrilliant();
	glPopMatrix();

	glPushMatrix();
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, silver);
	//glRotatef(angle, 0.0, 1.0, 0.0);
	glTranslatef(3.0, 0.0, 2.0);
	glBindTexture(GL_TEXTURE_2D, texnames[2]);
	drawSquare();
	glPopMatrix();

	glPushMatrix();
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, yellow);
	//glRotatef(angle, 0.0, 1.0, 0.0);
	glTranslatef(3.0, 0.0, -2.0);
	glBindTexture(GL_TEXTURE_2D, texnames[3]);
	drawHeart();
	glPopMatrix();

	glPushMatrix();
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, red);
	//glRotatef(angle, 0.0, 1.0, 0.0);
	glTranslatef(0.0, 0.0, -3.0);
	glBindTexture(GL_TEXTURE_2D, texnames[4]);
	drawMarquis();
	glPopMatrix();

	glPushMatrix();
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, cyan);
	//glRotatef(angle, 0.0, 1.0, 0.0);
	glTranslatef(-3.0, 0.0, -2.0);
	glBindTexture(GL_TEXTURE_2D, texnames[5]);
	drawTrilliant();
	glPopMatrix();

	glPushMatrix();
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, purple);
	//glRotatef(angle, 0.0, 1.0, 0.0);
	glTranslatef(-3.0, 0.0, 2.0);
	glBindTexture(GL_TEXTURE_2D, texnames[6]);
	drawSquare();
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glPopMatrix();

	glPopMatrix();

	checkError("drawScene");
	glutSwapBuffers();
}